FROM alpine:3.18.0

# Copying files
COPY ./sources /sources

RUN \
  # Install packages 
  apk add nginx php php-fpm wget \
  && mkdir /run/php-fpm \
  # Sorting files into folders
  && cp /sources/index.php /var/www \
  && cp /sources/nginx.conf /etc/nginx/nginx.conf \
  && cp /sources/www.conf /etc/php81/php-fpm.d/www.conf \
  # Create local SSL-certificates
  && wget -P /tmp https://dl.filippo.io/mkcert/latest\?for\=linux/amd64 \
  && mv /tmp/latest?for=linux%2Famd64 /tmp/mkcert \
  && cd /tmp \
  && chmod +x mkcert \
  && ./mkcert -install \
  && ./mkcert localhost

CMD php-fpm81 -D ; nginx
 
EXPOSE 443
