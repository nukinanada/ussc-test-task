# Build docker image
build:
	@docker build -t app-php .

# Run docker container
run:
	@docker run --name web_server -d -p 443:443 app-php

# Stop and delete docker container
clear:
	@docker stop web_server
	@docker container prune
